#include "p_graphics.h"

void set_solid_color(float r, float g, float b) {
    int f_x;
    int f_y;
    for (f_x = 0; f_x < frame_width; f_x = f_x + 1) {
        for (f_y = 0; f_y < frame_height; f_y = f_y + 1) {
            int pixel_index = get_index_3d(f_x, f_y, 0, frame_width, 3);
            frame[pixel_index + 0] = r;
            frame[pixel_index + 1] = g;
            frame[pixel_index + 2] = b;
        }
    }
}

void set_pixel_color(float r, float g, float b, int x, int y) {
    int pixel_index = get_index_3d(x, y, 0, frame_width, 3);
    frame[pixel_index + 0] = r;
    frame[pixel_index + 1] = g;
    frame[pixel_index + 2] = b;
}

void calculate_angles(int screen_width, int screen_height, float field_of_view) {
    int screen_size = screen_width*screen_height;
    angle_array = (float*) malloc(sizeof (float) * screen_size * 2);
    default_vector = vector_3d_create(1, 0, 0);
    float field_of_view_y = (screen_height * field_of_view) / screen_width;
    float begin_x = -field_of_view / 2;
    float begin_y = -field_of_view_y / 2;
    int x, y;
    for (x = 0; x < screen_width; x = x + 1) {
        for (y = 0; y < screen_height; y = y + 1) {
            int pixel_index = get_index_3d(x, y, 0, screen_width, 2);
            float mu_x = (float) x / (float) screen_width;
            float angle_value_x = begin_x + mu_x*field_of_view;
            float mu_y = (float) y / (float) screen_height;
            float angle_value_y = begin_y + mu_y*field_of_view_y;
            angle_array[pixel_index] = angle_value_y;
            angle_array[pixel_index + 1] = -angle_value_x;
        }
    }
}

int next_octant(vector_3d* hit_position, vector_3d* ray_direction) {
    return 0;
}

bool render_point(octree* test, vector_3d* ray_pos, vector_3d* ray_angle, int traverse_depth) {
    float t_distance;
    float tmin;
    float tmax;
    bool hit = octree_intersect(test, ray_pos, ray_angle, &t_distance, &tmin, &tmax);
    if (!hit) {
        return false;
    }
    vector_3d add_pos = vector_3d_vector_multiply(ray_angle, tmin);
    vector_3d hit_pos = vector_3d_vector_add(ray_pos, &add_pos);
    float t_diff = fmax(test->half_dimension.x, fmax(test->half_dimension.y, test->half_dimension.z))*2;
    int steps = (int) pow(2, traverse_depth);
    float step_interval = (float) ((float) t_diff / (float) (steps));
    int i;
    bool display = false;
    for (i = 0; i < steps; i = i + 1) {
        float check_length = step_interval * (float) i;
        vector_3d check_add = vector_3d_vector_multiply(ray_angle, check_length);
        vector_3d check_pos = vector_3d_vector_add(&hit_pos, &check_add);
        if (octree_check(test, check_pos) != true) {
            break;
        }
        octree* traverse_test = test;
        int j;
        for (j = 0; j < traverse_depth; j = j + 1) {
            if (octree_is_leaf(traverse_test)) {
                break;
            } else {
                int traverse_oct = octree_get_octant(traverse_test, check_pos);
                traverse_test = traverse_test->children[traverse_oct];
            }
        }
        if (traverse_test->data != NULL) {
            display = true;
            break;
        }
    }
    return display;
}

void cpu_render_point(int id, float* angle_array, int* render_width, int* render_height, float* test, vector_3d* ray_pos, vector_3d* ray_angle, int* traverse_depth, float* render_buffer) {
    int i = id;
    float divide = ((float) i) / ((float) *render_width);
    int p_y = (int) (floor(divide));
    float sum = (float) p_y * (float) (*render_width);
    int p_x = i - (int) sum;
    int r_w = *render_width;
    int r_h = *render_height;
    int depth = *traverse_depth;
    int pixel_index = ((p_y * r_w + p_x) * 3) + 0;
    render_buffer[pixel_index] = 0;
    render_buffer[pixel_index + 1] = 0;
    render_buffer[pixel_index + 2] = 1;
    int angle_pixel_index = ((p_y * r_w + p_x) * 2) + 0;
    float cam_angle_x_deg = angle_array[angle_pixel_index];
    float cam_angle_y_deg = angle_array[angle_pixel_index + 1];
    vector_3d base_ray_pos = *ray_pos;
    vector_3d base_ray_angle = *ray_angle;
    vector_3d normalize_ray_angle;
    float ray_magnitude = sqrt((base_ray_angle.x * base_ray_angle.x) + (base_ray_angle.y * base_ray_angle.y) + (base_ray_angle.z * base_ray_angle.z));
    normalize_ray_angle.x = base_ray_angle.x / ray_magnitude;
    normalize_ray_angle.y = base_ray_angle.y / ray_magnitude;
    normalize_ray_angle.z = base_ray_angle.z / ray_magnitude;
    float n_x_o = normalize_ray_angle.x;
    float n_y_o = normalize_ray_angle.y;
    float n_z_o = normalize_ray_angle.z;
    float n_rad = (float) cam_angle_x_deg * (float) ((float) 3.14 / (float) 180);
    float n_x = (cos(n_rad) * n_x_o) - (sin(n_rad) * n_y_o);
    float n_y = (sin(n_rad) * n_x_o) + (cos(n_rad) * n_y_o);
    float n_z = n_z_o;
    float n_rad_2 = (float) cam_angle_y_deg * (float) ((float) 3.14 / (float) 180);
    float n_x_2 = (cos(n_rad_2) * n_x) + (sin(n_rad_2) * n_z);
    float n_y_2 = n_y;
    float n_z_2 = (cos(n_rad_2) * n_z) - (sin(n_rad_2) * n_x);
    vector_3d result_dir;
    result_dir.x = n_x_2;
    result_dir.y = n_y_2;
    result_dir.z = n_z_2;
    vector_3d r_origin = base_ray_pos;
    vector_3d r_dir = result_dir;
    vector_3d dirfrac;
    dirfrac.x = 1.0f / r_dir.x;
    dirfrac.y = 1.0f / r_dir.y;
    dirfrac.z = 1.0f / r_dir.z;
    vector_3d lb, rt;
    lb.x = test[1] - test[4];
    lb.y = test[2] - test[5];
    lb.z = test[3] - test[6];
    rt.x = test[1] + test[4];
    rt.y = test[2] + test[5];
    rt.z = test[3] + test[6];
    float t1 = (lb.x - r_origin.x) * dirfrac.x;
    float t2 = (rt.x - r_origin.x) * dirfrac.x;
    float t3 = (lb.y - r_origin.y) * dirfrac.y;
    float t4 = (rt.y - r_origin.y) * dirfrac.y;
    float t5 = (lb.z - r_origin.z) * dirfrac.z;
    float t6 = (rt.z - r_origin.z) * dirfrac.z;
    float tmin = fmaxf(fmaxf(fminf(t1, t2), fminf(t3, t4)), fminf(t5, t6));
    float tmax = fminf(fminf(fmaxf(t1, t2), fmaxf(t3, t4)), fmaxf(t5, t6));
    bool ray_hit;
    float t;
    if (tmax < 0 || tmin > tmax) {
        t = tmax;
        ray_hit = false;
    } else {
        t = tmin;
        ray_hit = true;
    }

    if (ray_hit) {
        vector_3d add_pos;
        add_pos.x = ray_angle->x * tmin;
        add_pos.y = ray_angle->y * tmin;
        add_pos.z = ray_angle->z * tmin;
        vector_3d hit_pos;
        hit_pos.x = ray_pos->x + add_pos.x;
        hit_pos.y = ray_pos->y + add_pos.y;
        hit_pos.z = ray_pos->z + add_pos.z;
        float t_diff = fmaxf(test[4], fmaxf(test[5], test[6]))*2;
        int steps = 1;
        int a;
        for (a = 0; a < depth; a = a + 1) {
            steps = steps * 2;
        }
        float step_interval = (float) ((float) t_diff / (float) (steps));
        int i;
        bool display = false;
        for (i = 0; i < steps; i = i + 1) {
            float check_length = step_interval * (float) i;
            vector_3d check_add;
            check_add.x = ray_angle->x * check_length;
            check_add.y = ray_angle->y * check_length;
            check_add.z = ray_angle->z * check_length;
            vector_3d check_pos;
            check_pos.x = hit_pos.x + check_add.x;
            check_pos.y = hit_pos.y + check_add.y;
            check_pos.z = hit_pos.z + check_add.z;
            float allowance = 0.5;
            vector_3d bound_min;
            vector_3d bound_max;
            bound_min.x = test[1] - test[4];
            bound_min.y = test[2] - test[5];
            bound_min.z = test[3] - test[6];
            bound_max.x = test[1] + test[4];
            bound_max.y = test[2] + test[5];
            bound_max.z = test[3] + test[6];
            float pos_x = check_pos.x;
            float pos_y = check_pos.y;
            float pos_z = check_pos.z;
            if
                (
                    true
                    ) {
                ////////////////////////////////////////
                render_buffer[pixel_index] = 1;
                render_buffer[pixel_index + 1] = 0;
                render_buffer[pixel_index + 2] = 0;
                return; 
                int traverse_test_start_index = 0;
                int traverse_test_level = 1;
                int traverse_test_octant = 8;
                int j;
                for (j = 0; j < depth; j = j + 1) {
                    int traverse_test_next_size = (float) 14 * (float) pow((float) 8, (float) (j + 1));
                    int traverse_test_next_octant_size = traverse_test_next_size / 8;
                    int level_start_index = 0;
                    int s_d;
                    for (s_d = 0; s_d < j; s_d = s_d + 1) {
                        int add_amt = (float) 14 * (float) pow((float) 8, (float) (s_d));
                        level_start_index = level_start_index + add_amt;
                    }
                    int leaf_check_index = level_start_index + (traverse_test_octant * traverse_test_next_octant_size);
                    if (test[leaf_check_index] == 0) {
                        break;
                    } else {
                        int traverse_oct = 0;
                        if (check_pos.x >= test[traverse_test_start_index + 1]) {
                            traverse_oct |= 4;
                        }
                        if (check_pos.y >= test[traverse_test_start_index + 2]) {
                            traverse_oct |= 2;
                        }
                        if (check_pos.z >= test[traverse_test_start_index + 3]) {
                            traverse_oct |= 1;
                        }
                        traverse_test_start_index = level_start_index + (traverse_oct * traverse_test_next_octant_size);
                    }
                }
                if (test[traverse_test_start_index] == 1) {
                    render_buffer[pixel_index] = 1;
                    render_buffer[pixel_index + 1] = 0;
                    render_buffer[pixel_index + 2] = 0;
                    break;
                }
            } else {
                break;
            }
        }
    }
}

void renderAABB(float* render_frame, int render_width, int render_height, camera* render_cam, world* render_scene, int traverse_depth) {
    int x, y;
    int scene_octrees = render_scene->octrees.size;
    vector_3d normal_rotation = vector_3d_normalize(&render_cam->rotation);
    //printf("i am here\n");
    for (x = 0; x < render_width; x = x + 1) {
        for (y = 0; y < render_height; y = y + 1) {
            int angle_pixel_index = ((y * render_width + x) * 2) + 0;
            float cam_angle_x_deg = angle_array[angle_pixel_index];
            float cam_angle_y_deg = angle_array[angle_pixel_index + 1];
            vector_3d* rot1 = rotate_degree_z(&normal_rotation, cam_angle_x_deg);
            vector_3d* ray_angle = rotate_degree_y(rot1, cam_angle_y_deg);
            int pixel_index = ((y * render_width + x) * 3) + 0;
            bool hit = false;
            int o;
            for (o = 0; o < scene_octrees; o = o + 1) {
                octree* test;
                if (o >= render_scene->octrees.size || o < 0) {
                    printf("o: %d out of range\nmax render_scene->octree.size: %d\n", o, render_scene->octrees.size);
                    printf("Unable to return value - exiting...\n");
                    exit(-1);
                } else {
                    test = render_scene->octrees.data[o];
                }
                hit = render_point(test, &render_cam->position, ray_angle, 5);
            }
            if (hit) {
                render_frame[pixel_index] = 1;
                render_frame[pixel_index + 1] = 0;
                render_frame[pixel_index + 2] = 0;
            } else {
                render_frame[pixel_index] = 0;
                render_frame[pixel_index + 1] = 0;
                render_frame[pixel_index + 2] = 0;
            }
            free(rot1);
            free(ray_angle);
        }
    }
}

void cl_renderAABB(float* render_frame, int render_width, int render_height, camera* cam, world* scene) {
    size_t frame_size = render_width * render_height;
    int world_size = scene->octrees.size;
    int i;
    for (i = 0; i < world_size; i = i + 1) {
        octree* render_octree = (octree*) vector_pointer_get(&scene->octrees, i);
        //printf("render_octree is %p\n", render_octree);
        int o_size;
        float* test = octree_to_linear_static_array(render_octree, 5, &o_size);
        /*
        //CPU render
        int count;
        for (count = 0; count < frame_size; count = count + 1) {
            cpu_render_point(count, angle_array, &frame_width, &frame_height, test, &cam->position, &cam->rotation, &octree_traverse_depth, render_frame);
        }
         * */
        //GPU render
        clEnqueueWriteBuffer(command_queue, test_d, CL_TRUE, 0, octree_max_size * sizeof (float), test, 0, NULL, NULL);
        clFinish(command_queue);
        clEnqueueWriteBuffer(command_queue, ray_pos_d, CL_TRUE, 0, sizeof (vector_3d), &cam->position, 0, NULL, NULL);
        clFinish(command_queue);
        clEnqueueWriteBuffer(command_queue, ray_angle_d, CL_TRUE, 0, sizeof (vector_3d), &cam->rotation, 0, NULL, NULL);
        clFinish(command_queue);
        clSetKernelArg(kernel, 3, sizeof (cl_mem), &test_d);
        clSetKernelArg(kernel, 4, sizeof (cl_mem), &ray_pos_d);
        clSetKernelArg(kernel, 5, sizeof (cl_mem), &ray_angle_d);
        clEnqueueNDRangeKernel(command_queue, kernel, 1, 0, &frame_size, 0, 0, NULL, NULL);
        clFinish(command_queue);
        clEnqueueReadBuffer(command_queue, render_buffer_d, CL_TRUE, 0, frame_size * 3 * sizeof(float), render_frame, 0, NULL, NULL);
        clFinish(command_queue);
    }
}