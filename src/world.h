#ifndef world_h
#define world_h
#include <stdlib.h>
#include <stdio.h>
#include "vector_pointer.h"

typedef struct {
    vector_pointer octrees;
} world;
void world_init(world* scene, vector_pointer* octrees);
void world_init_default(world* scene);
world* world_create(vector_pointer* octrees);
world* world_create_default();
#endif
