#ifndef octree_h
#define octree_h
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "vector_3d.h"
#include "p_util.h"
#include "vector_pointer.h"
typedef struct octree octree;

struct octree {
    vector_3d origin;
    vector_3d half_dimension;
    octree* children[8];
    vector_3d* data;
};
void octree_init(octree* o, vector_3d* origin, vector_3d* half_dimension);
void octree_init_default(octree* o);
octree* octree_create(vector_3d* origin, vector_3d* half_dimension);
octree* octree_create_default();
vector_3d octree_get_full_dimension(octree* o);
int octree_get_octant(octree* o, vector_3d point);
bool octree_is_leaf(octree* o);
void octree_insert(octree*o, vector_3d* p);
void octree_get_box_points(octree* o, vector_3d bmin, vector_3d bmax, vector_pointer* results);
bool octree_check(octree* o, vector_3d p);
bool octree_intersect(octree* o, vector_3d* origin, vector_3d* direction, float* t_out, float* t_min_out, float* t_max_out);
octree* octree_binvox(char* filename, float origin_x, float origin_y, float origin_z);
float* octree_to_linear_static_array(octree* o, int depth, int* size);
#endif
