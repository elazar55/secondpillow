#ifndef util_h
#define util_h
#include <math.h>
#include "vector_3d.h"
int get_index_2d(int x, int y, int wide);
int get_index_3d(int x, int y, int z, int wide, int thick);
float linear_interpolate(float a, float b, float mu);
float to_radian(float degree);
float to_degree(float radian);
extern int read_binvox_c(char* filename, int* o_version, int* o_depth, int* o_height, int* o_width, int* o_size, unsigned char* o_voxels, float* o_tx, float* o_ty, float* o_tz, float* o_scale);
extern unsigned char* read_binvox_ii_c(char* filename, int* dim_x, int* dim_y, int* dim_z, float* translate_x, float* translate_y, float* translate_z, float* scale_factor);
vector_3d* get_binvox_coord(int index, int wide, int high, int thick);
#endif
