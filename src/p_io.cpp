#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

const char* read_file(const char* filename) {
    string line;
    string text = "";
    ifstream file(filename);
    cout << "opening " << filename << endl;
    if (file.is_open()) {
        while (getline(file, line)) {
            text += line;
            text += "\n";
            //cout << line << endl;
        }
        file.close();
    } else {
        cout << "unable to open file\n";
    }
    return text.c_str();
}

extern "C" const char* read_file_c(const char* filename) {
    const char* file_contents = read_file(filename);
    return file_contents;
}