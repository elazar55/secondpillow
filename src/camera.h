#ifndef camera_h
#define camera_h
#include "vector_3d.h"

typedef struct {
    vector_3d position;
    vector_3d rotation;
} camera;
void camera_init(camera* c, vector_3d position, vector_3d rotation);
void camera_init_default(camera* c);
camera* camera_create(vector_3d position, vector_3d rotation);
camera* camera_create_default();
#endif
