#include "sphere.h"

void sphere_init(sphere* s, vector_3d* origin, float radius) {
    s = (sphere*) malloc(sizeof (sphere));
    s->origin = origin;
    s->radius = radius;
}

void sphere_init_default(sphere* s) {
    vector_3d* s_origin = vector_3d_create_default();
    float s_radius = 1.0;
    sphere_init(s, s_origin, s_radius);
}

sphere* sphere_create(vector_3d* origin, float radius) {
    sphere* s;
    sphere_init(s, origin, radius);
    return s;
}

sphere* sphere_create_default() {
    sphere* s;
    sphere_init_default(s);
    return s;
}

vector_3d sphere_get_normal(sphere* s, vector_3d* point) {
    vector_3d* s_center = s->origin;
    vector_3d cent_neg = vector_3d_negative(s->origin);
    vector_3d neg_normal = vector_3d_normalize(&cent_neg);
    vector_3d normal_vector = vector_3d_vector_add(point, &neg_normal);
    return normal_vector;
}

float sphere_intersect(sphere* s, vector_3d* r_origin, vector_3d* r_dir) {
    float r_origin_x = r_origin->x;
    float r_origin_y = r_origin->y;
    float r_origin_z = r_origin->z;
    float r_dir_x = r_dir->x;
    float r_dir_y = r_dir->y;
    float r_dir_z = r_dir->z;
    float s_origin_x = s->origin->x;
    float s_origin_y = s->origin->y;
    float s_origin_z = s->origin->z;
    float a = 1;
    float b = (2 * (r_origin_x - s_origin_x) * r_dir_x);
    float c = pow(r_origin_x - s_origin_x, 2) + pow(r_origin_y - s_origin_y, 2) + pow(r_origin_z - s_origin_z, 2) - s->radius * s->radius;
    float discriminant = b * b* -4 * c;
    if (discriminant > 0) {
        double root_1 = ((-1 * b - sqrt(discriminant)) / 2) - 0.000001;
        if (root_1 > 0) {
            return root_1;
        } else {
            double root_2 = ((sqrt(discriminant) - b) / 2) - 0.000001;
            return root_2;
        }
    } else {
        return -1;
    }
}