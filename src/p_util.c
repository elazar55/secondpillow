#include "p_util.h"
#include "vector_3d.h"

int get_index_2d(int x, int y, int wide) {
    return y * wide + x;
}

int get_index_3d(int x, int y, int z, int wide, int thick) {
    return ((y * wide + x) * thick) +z;
}

float linear_interpolate(float a, float b, float mu) {
    return a * (1 - mu) + b * mu;
}

vector_3d* get_binvox_coord(int index, int wide, int high, int thick) {
    int x, y, z;
    x = index / (wide * high);
    int zw = index % (wide * high);
    z = zw / wide;
    y = zw % wide;
    vector_3d* c = vector_3d_create(x, y, z);
    return c;
}