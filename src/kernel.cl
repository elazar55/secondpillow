typedef struct
{
	float x;
	float y;
	float z;
}vector_3d;
float fminf(float x, float y)
{
	if (y < x)
	{
		return y;
	}
	else
	{
		return x;
	}
}
float fmaxf(float x, float y)
{
	if (y > x)
	{
		return y;
	}
	else
	{
		return x;
	}
}
//fabrice navidad
//one gigantic render function
__kernel void cl_render_point(__global float* angle_array, __global int* render_width, __global int* render_height, __global float* test, __global vector_3d* ray_pos, __global vector_3d* ray_angle, __global int* traverse_depth, __global float* render_buffer)
{ 
    int i = get_global_id(0);
    float divide = ((float)i)/((float)*render_width);
    int p_y = (int)(floor(divide));
    float sum = (float)p_y * (float)(*render_width);
    int p_x = i - (int)sum;
    int r_w = *render_width;
    int r_h = *render_height;
    int depth = *traverse_depth;
    int pixel_index = ((p_y*r_w + p_x) * 3) + 0;
    render_buffer[pixel_index] = 0;
    render_buffer[pixel_index + 1] = 0;
    render_buffer[pixel_index + 2] = 1;
    int angle_pixel_index = ((p_y*r_w + p_x) * 2) + 0;
    float cam_angle_x_deg = angle_array[angle_pixel_index];
    float cam_angle_y_deg = angle_array[angle_pixel_index + 1];
    vector_3d base_ray_pos = *ray_pos;
    vector_3d base_ray_angle = *ray_angle;
    vector_3d normalize_ray_angle;
    float ray_magnitude = sqrt((base_ray_angle.x * base_ray_angle.x) + (base_ray_angle.y * base_ray_angle.y) + (base_ray_angle.z * base_ray_angle.z));
    normalize_ray_angle.x = base_ray_angle.x/ray_magnitude;
    normalize_ray_angle.y = base_ray_angle.y/ray_magnitude;
    normalize_ray_angle.z = base_ray_angle.z/ray_magnitude;
    float n_x_o = normalize_ray_angle.x;
    float n_y_o = normalize_ray_angle.y;
    float n_z_o = normalize_ray_angle.z;
    float n_rad = (float)cam_angle_x_deg*(float)((float)3.14/(float)180);
    float n_x = (cos(n_rad)*n_x_o) - (sin(n_rad)*n_y_o);
    float n_y = (sin(n_rad)*n_x_o) + (cos(n_rad)*n_y_o);
    float n_z = n_z_o;
    float n_rad_2 = (float)cam_angle_y_deg*(float)((float)3.14/(float)180);
    float n_x_2 = (cos(n_rad_2)*n_x) + (sin(n_rad_2)*n_z);
    float n_y_2 = n_y;
    float n_z_2 = (cos(n_rad_2)*n_z) - (sin(n_rad_2)*n_x);
    vector_3d result_dir;
    result_dir.x = n_x_2;
    result_dir.y = n_y_2;
    result_dir.z = n_z_2;
    vector_3d r_origin = base_ray_pos;
    vector_3d r_dir = result_dir;
    vector_3d dirfrac;
    dirfrac.x = 1.0f / r_dir.x;
    dirfrac.y = 1.0f / r_dir.y;
    dirfrac.z = 1.0f / r_dir.z;
    vector_3d lb, rt;
    lb.x = test[1] - test[4];
    lb.y = test[2] - test[5];
    lb.z = test[3] - test[6];
    rt.x = test[1] + test[4];
    rt.y = test[2] + test[5];
    rt.z = test[3] + test[6];
    float t1 = (lb.x - r_origin.x)*dirfrac.x;
    float t2 = (rt.x - r_origin.x)*dirfrac.x;
    float t3 = (lb.y - r_origin.y)*dirfrac.y;
    float t4 = (rt.y - r_origin.y)*dirfrac.y;
    float t5 = (lb.z - r_origin.z)*dirfrac.z;
    float t6 = (rt.z - r_origin.z)*dirfrac.z;
    float tmin = fmaxf(fmaxf(fminf(t1, t2), fminf(t3, t4)), fminf(t5, t6));
    float tmax = fminf(fminf(fmaxf(t1, t2), fmaxf(t3, t4)), fmaxf(t5, t6));
    bool ray_hit;
    float t;
    if (tmax < 0 || tmin > tmax)
    {
    	t = tmax;
    	ray_hit = false;
    }
    else
    {
    	t = tmin;
    	ray_hit = true;
    }

    if (ray_hit)
    {
    	vector_3d add_pos;
    	add_pos.x = ray_angle->x * tmin;
    	add_pos.y = ray_angle->y * tmin;
    	add_pos.z = ray_angle->z * tmin;
    	vector_3d hit_pos;
    	hit_pos.x = ray_pos->x + add_pos.x;
    	hit_pos.y = ray_pos->y + add_pos.y;
    	hit_pos.z = ray_pos->z + add_pos.z;
    	float t_diff = fmaxf(test[4], fmaxf(test[5], test[6]))*2;
    	int steps = 1;
    	int a;
    	for (a = 0; a < depth; a = a + 1)
    	{
    		steps = steps * 2;
    	}
    	
    	float step_interval = (float)((float)t_diff/(float)(steps));
    	int i;
    	bool display = false;
    	for (i = 0; i < steps; i = i + 1)
    	{
            float check_length = step_interval * (float)i;
            vector_3d check_add;
            check_add.x = ray_angle->x * check_length;
            check_add.y = ray_angle->y * check_length;
            check_add.z = ray_angle->z * check_length;
            vector_3d check_pos;
            check_pos.x = hit_pos.x + check_add.x;
            check_pos.y = hit_pos.y + check_add.y;
            check_pos.z = hit_pos.z + check_add.z;
            float allowance = 0.1;
            vector_3d bound_min;
            vector_3d bound_max;
            bound_min.x = test[1] - test[4];
            bound_min.y = test[2] - test[5];
            bound_min.z = test[3] - test[6];
            bound_max.x = test[1] + test[4];
            bound_max.y = test[2] + test[5];
            bound_max.z = test[3] + test[6];
            float pos_x = check_pos.x;
            float pos_y = check_pos.y;
            float pos_z = check_pos.z;
            if
            (
            true
            )
            {
                int traverse_test_start_index = 0;
                int traverse_test_level = 1;
                int traverse_test_octant = 8;
		int j;
		for (j = 0; j < depth; j = j + 1)
		{
                    int traverse_test_next_size = (float) 14 * (float) pow((float) 8, (float) (j+1));
                    int traverse_test_next_octant_size = traverse_test_next_size / 8;
                    int level_start_index = 0;
                    int s_d;
                    for (s_d = 0; s_d < j; s_d = s_d + 1) {
                        int add_amt = (float) 14 * (float) pow((float) 8, (float) (s_d));
                        level_start_index = level_start_index + add_amt;
                    }
                    int leaf_check_index = level_start_index + (traverse_test_octant * traverse_test_next_octant_size);
                    if (test[leaf_check_index] == 0)
                    {
			break;
                    }
                    else
                    {
                            int traverse_oct = 0;
                            if (check_pos.x >= test[traverse_test_start_index + 1])
                            {
                                    traverse_oct |= 4;
                            }
                            if (check_pos.y >= test[traverse_test_start_index + 2])
                            {
                                    traverse_oct |= 2;
                            }
                            if (check_pos.z >= test[traverse_test_start_index + 3])
                            {
                                    traverse_oct |= 1;
                            }
                            traverse_test_start_index = level_start_index + (traverse_oct * traverse_test_next_octant_size);
                    }
                }
                if (test[traverse_test_start_index] == 1)
                {
                        render_buffer[pixel_index] = 1;
                        render_buffer[pixel_index + 1] = 0;
                        render_buffer[pixel_index + 2] = 0;
                        break;
                }
            } 
            else
            {
                    break;
            }
    	}
    }
}